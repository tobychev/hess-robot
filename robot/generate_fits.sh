#/usr/bin/env bash
if test "$BASH" = "" || "$BASH" -uc "a=();true \"\${a[@]}\"" 2>/dev/null; then
    # Bash 4.4, Zsh
    set -euo pipefail
else
    # Bash 4.3 and older chokes on empty arrays with set -u.
    set -eo pipefail
fi
shopt -s nullglob globstar

# -allow a command to fail with !’s side effect on errexit
# -use return value from ${PIPESTATUS[0]}, because ! hosed $?
! getopt --test > /dev/null 
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
    echo 'I’m sorry, `getopt --test` failed in this environment.'
    exit 1
fi

usage ()
{
    echo "usage: generate_fits.sh sources.ini [-d] [options]"
    echo "  First argument must be the source ini file"
    echo "  Internal tool, not really for manual use "
	 echo "  -d                                  debug"
    exit 1	
}

debug_print () {
   if [ "$1" == y ]; then
      printf "Doing %s" "$3"
      if [ $2 -ne  0 ]; then
         printf "Encountered an error during, halting." "$3"
         exit -1
      fi
      printf "\n"
   fi

}

choose_setup(){
   listname=$1
   hapConf=$2

   reg_hess1=".*_hess1_runs.list"
   reg_hess2=".*_hess2_?.*_runs.list"
   reg_hess1u=".*_hess1u_?.*_runs.list"
   reg_hylist=".*hybrid.*"
   reg_hyconf=".*hybrid"

   hyconf=n
   hylist=n

   if [[ $listname =~ $reg_hylist ]]; then
      hylist=y
   fi
   if [[ $hapConf =~ $reg_hyconf ]]; then
      hyconf=y
   fi

   if [ "$hylist" == "n" ]; then
      if [ "$hyconf" == "y" ]; then
         export hapConf=${hapConf/_hybrid/}
      fi
   fi

   if [[ $listname =~ $reg_hess1 ]]; then
      printf "Setting HESS1 env paths.\n"
      setup_hess1
      return 0
   fi
   if [[ $listname =~ $reg_hess2 ]]; then
      printf "Setting HESS2 env paths.\n"
      setup_hess2
      return 0
   fi
   if [[ $listname =~ $reg_hess1u ]]; then
      printf "Setting HESS1u env paths.\n"
      setup_hess1u
      return 0
   fi
}

setup_hess1(){
   export HESSCONFIG=/lfs/l7/hess/analysis/version36
   export HESSDST=/lfs/l2/hess/dstdata_extended_13-06
   export HESSDATA=/lfs/l2/hess/hddata/
}

setup_hess2(){
   export HESSCONFIG=/lfs/l7/hess/analysis/version36
   export HESSDST=/lfs/l2/hess/users/marandon/CalibData/CT5/dst_WithSimpleGainMerge_NewBrkCard_AllDT_extended2B_FallbackCalib
   export HESSDATA=/lfs/l7/hess/users/marandon/CalibData/CT5/hddata_WithSimpleGainMerge_NewBrkCard2/
}

setup_hess1u(){
   export HESSCONFIG=/lfs/l7/hess/analysis/version36
   export HESSDST=/lfs/l7/hess/users/marandon/CalibData/CT5/dst_NewScheme
   export HESSDATA=/lfs/l7/hess/users/marandon/CalibData/CT5/hddata_NewScheme/
}

STARTDIR="$(pwd)"

#  First thing given must be the source ini file
SRCINI="$1"
shift

LONGOPTS=debug,loc:,srcdir:,srcname:

# -temporarily store output to be able to check for errors
# -activate quoting/enhanced mode (e.g. by writing out “--options”)
# -pass arguments only via   -- "$@"   to separate them correctly
! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # e.g. return value is 1
    #  then getopt has complained about wrong arguments to stdout
    exit 2
fi
# read getopt’s output this way to handle the quoting right:
eval set -- "$PARSED"

debug=n dir=n name=n 
srcDir=- srcName=- robotdir=- 
# now enjoy the options in order and nicely split until we see --

while true; do
    case "$1" in
        --debug)
            debug=y
            shift
            ;;
        --loc)
            robotdir="$2"
            shift 2
            ;;
        --srcdir)
				dir=y
            srcDir="$2"
            shift 2
            ;;
        --srcname)
            name=y
            srcName="$2"
            shift 2
            ;;
        --)
            shift
				if [ ${#PARSED} -eq 3 ]; then
					usage
				fi
				if [ ! -z "${1+x}" ]; then
					echo "Unknown option $1"
					usage
				fi
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

if [ "$debug" == y ]; then
   printf "Running generate_fits\n"
   printf "Python version: %s \n" $(which python)
fi

## Parse source config section ##
ops=$(python $robotdir/robot/hess_robot.py "$SRCINI" --parse-section-options "$srcName" "HapConfig" "CondaEnvName" "CondaRootDir" "RunListDir" )
sep='#'
source_options=()
while read -rd "$sep" i; do
    source_options+=("$i")
done < <(printf '%s%s' "$ops" "$sep")

maxHapConf="${source_options[0]}"
condaenv="${source_options[1]}"
condadir="${source_options[2]}"
runlistdir="${source_options[3]}"

if [ ! -d "$srcDir/fits_export" ]; then
   if [ "$debug" == y ]; then
      printf "Creating destination dir %s/fits_export\n" "$srcDir"
   fi
   mkdir "$srcDir/fits_export"
fi

runlists=$(ls -1 "$runlistdir" | grep -v "Complete" | grep "$srcName" | tr '\n' ' ')
runlists="$runlists " 

if [ "$debug" == y ]; then
   printf "Found runlists %s in %s matching name %s\n" "$runlists" "$runlistdir" "$srcName"
fi

#### ACTIVATE ENVIRONMENT ####
source ~hess/hap-18-11.sh > /dev/null

source "$condadir/etc/profile.d/conda.sh"
conda activate $condaenv 

if [ $? -ne 0 ]; then 
   printf "ERROR: could not activate the $condaenv environment, halting.\n"
   exit -1
fi

cd "$srcDir/fits_export"
while read -rd " " lst; do
   
   list="$runlistdir/$lst"
   kind=${lst//$srcName/}
   kind=${kind//.list/}
   # Choose setup sets hapConf basede on runlist
   choose_setup $list $maxHapConf
   if [ -z "$kind" ]; then
      break
   fi
   if [ "$debug" == y ]; then
      printf "Using config %s with runlist %s\n"  "$hapConf" "$list"
      printf "HESSROOT=%s\nHESSDST=%s\nHESSDATA=%s\n" "$HESSROOT" "$HESSDST" "$HESSDATA"
   fi

   runs=$(wc -l < $list) 
   if [ -d "events$kind/" ]; then
      fits=$(find "events$kind/" -name "*fits" | wc -l)
      mv "events$kind/" "events/"
   else
      fits=-1
   fi

   if [ "$debug" == y ]; then
      printf "runlist is %s long, fount %s fitsfiles\n" "$runs" "$fits"
   fi
   if [ "$runs" == "$fits" ]; then 
      printf "Found %s out of %s expected fits-files in %s, skipping this step. 
      Remove contents of events directory to force generation\n"  "$fits" "$runs" "events$kind/"
   else
      printf "Regenerating...\n"
      ("$robotdir/robot/hap-data-fits-export" --hap-config "$hapConf" --runs-file "$list" events --submit mpik > events$kind.log 2>&1)
      debug_print $debug $? "event export"
   fi
   
   ("$robotdir/robot/hap-data-fits-export" --hap-config "$hapConf" --runs-file "$list" irfs > irfs$kind.log 2>&1)
   debug_print $debug $? "irf export"
   ("$robotdir/robot/hap-data-fits-export" --hap-config "$hapConf" --runs-file "$list" reformat > reformat$kind.log 2>&1)
   debug_print $debug $? "reformat"
   ("$robotdir/robot/hap-data-fits-export" --hap-config "$hapConf" --runs-file "$list" obs-index > obs-index$kind.log 2>&1)
   debug_print $debug $? "observation index creation"
   ("$robotdir/robot/hap-data-fits-export" --hap-config "$hapConf" --runs-file "$list" hdu-index > hdu-index$kind.log 2>&1)
   debug_print $debug $? "hdu index creation"
   ("$robotdir/robot/hap-data-fits-export" --hap-config "$hapConf" --runs-file "$list" remove-bad-runs > remove-bad-runs$kind.log 2>&1)
   debug_print $debug $? "bad runs removal"
   state=$?
   mv out "out$kind"
   mv events "events$kind"
   mv irfs "irfs$kind"
   if [ "$debug" == y ]; then
      if [ $state -ne  0 ]; then
         echo "Encountered an error during export. Halting due to debug setting"
         exit -1
      fi
   fi
done < <(printf '%s' "$runlists")
conda deactivate

cd $STARTDIR
