#!/usr/bin/env python

import argparse as arg
import os
import pyrobot
import pyrobot.batch as ba
import pyrobot.config_handler as cf
import pyrobot.report as rt
import pyrobot.start as st
import pyrobot.utils as ut
import sys

if __name__ == "__main__":
   parser = arg.ArgumentParser(description='Automate analysis')
   parser.add_argument('config_file', action='store', type=str,
                  help='project configuration file')
   parser.add_argument('--generate-all', action="store_true",default=False,
                       help="Generate submit scripts for all sources in provided ini file")
   parser.add_argument('--generate-only', nargs=1,default=None,
                       help="Generate submit scripts for only source given")
   parser.add_argument('--check-runresult', nargs=1,default=None,
                       help="Check results of run")
   parser.add_argument('--generate-fitspectrum-config', nargs=2, default=None, dest="fitspectrum",
                       help="<config section> <result dir> \n  Generate FitSpectrum config for source given")
   parser.add_argument('--generate-hap-merge-list', nargs=2, default=None,
                       help="Generate comma separated list of files to merge")
   parser.add_argument('--generate-lightcurve-directory', nargs=2, default=None, dest="lightdir",
                       help="<source dir> <target dir>\n  Generate directory with all lightcurve files in one place")
   parser.add_argument('--generate-report', nargs=2, default=None,
                       help="<config section> <result dir> \n Generate a report for specified source")
   parser.add_argument('--parse-sources', action="store_true",default=False,
                       help="Parse config and return a list of sources")
   parser.add_argument('--parse-section', nargs=1, default=None,
                       help="Parse config and return properties for a section")
   parser.add_argument('--parse-section-options', nargs='+', default=None,
                       help="Parse config and return specific properties in a section")

   args = parser.parse_args()
   sources = cf.parse_sources_config(args.config_file)
   if len(sources) == 0:
      raise ValueError(f"Found only {len(sources)} sections in {args.config_file}")

   if args.generate_all:
      ut.make_dir(sources.defaults()["ProjRootDir"])
      ba.make_batch_files_from_config(sources)

   if args.generate_only:
      source = sources[args.generate_only[0]]
      script = ba.make_submission_script(source)

      filename = f"{source['SubmitScriptDir']}/submit_{source['name']}.sh"
      with open(filename,"w+") as fil:
         print(f"Writing {filename}")
         fil.writelines(script)
         mode = os.stat(filename).st_mode
         os.chmod(filename, mode | ba.stat.S_IXUSR | ba.stat.S_IXGRP | ba.stat.S_IXOTH)

   if args.check_runresult:
      # The unmangling here is a cheap hack to avoid solving the problem that we need sourcenames mangled for
      # filesystem lookup, but that this doesn't work to find the sections in the original config
      unmangled = list(filter(lambda x: ut.mangle_source_name(x) == args.check_runresult[0], sources.sections())).pop()
      errors, msg = ut.check_run_results(args.check_runresult[0],sources[unmangled])
      if errors == 0:
         print(f"{args.check_runresult[0]} Ok!")
      else:
         print(f"Found {errors} errors for {args.check_runresult[0]}.")
         with open(f"check_runresult_{args.check_runresult[0]}.log","w") as fil:
            fil.writelines(msg)


   if args.parse_sources:
      srcs = [ut.mangle_source_name(itm) for itm in sources.sections()]
      print (" ".join(srcs))

   if args.parse_section:
      src_name = ut.unmangle_name(args.parse_section[0],sources)
      ops = sorted(sources.options(src_name))
      
      values = [sources[src_name][val] for val in ops]
      print(ops)
      print ('#'.join(values))

   if args.parse_section_options:
      src_name = ut.unmangle_name(args.parse_section_options[0],sources)
      values = []
      for opt in args.parse_section_options[1:]:
         values.append(sources[src_name][opt])
      print ('#'.join(values))

   if args.generate_hap_merge_list:
      print (ut.list_hap_files_to_merge(*args.generate_hap_merge_list))

   if args.lightdir:
      # The unmangling here is a cheap hack to avoid solving the problem that we need sourcenames mangled for
      # filesystem lookup, but that this doesn't work to find the sections in the original config
      conf_src_name = ut.unmangle_name(args.lightdir[0],sources)
      src_name = args.lightdir[0]
      hap_config = sources[conf_src_name]["HapConfig"]
      src_dir = f"{sources[conf_src_name]['ProjRootDir']}/{src_name}"
      nfiles = ut.symlink_resultfiles_to_directory(hap_config, src_name, src_dir, args.lightdir[1])
      st.make_fake_start_runlist(src_name,args.lightdir[1],nfiles)
      st.make_start_config(src_name,sources[conf_src_name],args.lightdir[1])

   if args.fitspectrum:
      # The unmangling here is a cheap hack to avoid solving the problem that we need sourcenames mangled for
      # filesystem lookup, but that this doesn't work to find the sections in the original config
      unmangled = list(filter(lambda x: ut.mangle_source_name(x) == args.fitspectrum[0], sources.sections())).pop()
      conf_file, config = rt.make_fitspectrum_config(sources[unmangled],args.fitspectrum[0],args.fitspectrum[1])

      with open(conf_file,"w") as fil:
         fil.writelines(config)

      print(conf_file,flush=True)

   if args.generate_report:
      unmangled = list(filter(lambda x: ut.mangle_source_name(x) == args.generate_report[0], sources.sections())).pop()
      doc = rt.make_report(sources[unmangled],args.generate_report[0],args.generate_report[1])
      doc.save(f"{args.generate_report[1]}/{args.generate_report[0]}.docx")
      print(f"{args.generate_report[1]}/{args.generate_report[0]}.docx")
