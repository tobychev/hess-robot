import pyrobot
import pyrobot.utils as ut

import os

def make_fake_start_runlist(src_name,lightdir,nfiles):
   # Start needs(?) a list of files but hap-split gives simple
   # ordinal names so the real runlist can't be used
   with open(f"{lightdir}/{src_name}.list","w") as fil:
      for idx in range(0,nfiles):
         fil.write("{idx}  30\n")

def make_start_config(src_name,source_config,lightcurve_dir):

   conf_str="""
[User]
### Use input file list instead of run list (default is 0).
### This can be useful for MC studies.
UserUseInputFileList 0

### Run lists folder adress (absolute path)
UserRunListsFolderAdress {lightcurve_dir}

### Run list file name (will be automatically completed by .list)
UserRunListFileBaseName {src_name}

### Folder in which outputs are written (absolute path)
### if empty, nothing is saved
UserOutputFolderName 

### ROOT file output name (will be automatically completed by .root)
### Output will be saved as UserOutputFolderName/UserOutputRootFileName.root
UserOutputRootFileName start_{src_name}_spectra

[Data]
### Indivual runs root files folder adress (absolute path)
### (runs produced by the analysis and used as input for the spectrum)
UserRootFilesFolderAddress {lightcurve_dir}

### Set a cos(zenith) range max value : if the run cos(zen) distrib
### is larger than this value, the two zen bands will be used  
UserMaxCosZenBinWidth 0.05

### Set zenith max (compare to each zen band mean value. Skip part of run if necessary)
UserZenithMax 70.

### Set offset max (cut used for select runs)
UserOffsetMax 2.1

### Set minimal time (seconds) to consider a zen and offset band
UserTmin 200.

[Analysis]
### A label for the user analysis configuration (azimuth, cuts, analysis type)
### this is used for output names and also for collection area and resolution access
UserAnalysisConfig {HapConfig}

### Config type (unused at present: use preferably UserMcProductionName)
UserAnalysisConfigType default

### Set a MIN and MAX energy value participating to the fit (TeV)
UserFitEMin {LCEmin}
UserFitEMax {LCEmax}

### Set fsp energy range MIN value (TeV) 
### MUST be set or else things don't work ###
UserERangeMin {LCEmin}

### Set fsp energy range MAX value
UserERangeMax {LCEmax}

### Set fsp energy number of bins
UserERangeBinsNumber 3

### Option to compute contours
UserComputeContours 0

[Hypothesis]
UserAddHypothesis {LCEFluxLaw}

### To define the reference energy (TeV)
UserEref {LCEref}

#### To define the integration range for integrated hypothesis
UserHypothesisIntegrationEnergyRange {LCEmin},{LCEmax}

[LightCurve]
### Time cutting type
UserLightCurveTimeCuttingType {TimeBinType}

#### Time range used for light curve in MJD time
#### Interval have to be separated by a coma
#### For night by night add a 0.5 to MJD start and end
UserLightCurveTimeRange {StartMJD},{EndMJD}

#### Integrated flux energy range (TeV)
UserLightCurveIntegratedFluxEnergyRange {LCEmin},{LCEmax}

#### Light curves plot time axis units
UserLightCurveTimeAxisUnits 0

###UserLightCurveErrorsHandling 0
UserLightCurveErrorsHandling 1
"""
   conffile = f"{lightcurve_dir}/{src_name}_start.conf" 
   if not os.path.exists(conffile):
      with open(conffile,"w") as fil:
         conf = conf_str.format(
            lightcurve_dir = lightcurve_dir,
            src_name = src_name,
            HapConfig = source_config["HapConfig"],
            TimeBinType = source_config["TimeBinType"],
            StartMJD = source_config["StartMJD"],
            EndMJD = source_config["EndMJD"],
            LCEmin = source_config["LCEmin"],
            LCEmax = source_config["LCEmax"],
            LCEref = source_config["LCEref"],
            LCEFluxLaw = source_config["LCEFluxLaw"],
            )
         fil.writelines(conf)
