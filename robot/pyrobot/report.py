import argparse 
import collections
import datetime as dt
import json
import os
import subprocess as sub
import shutil as sh
import docx as dx

Paths = collections.namedtuple("Paths","file src_filename res_dir")



def add_map_plots(paths,doc,pngs):
   plot_types = ["skymaps", "totaldists", "runwise"]

   plot_names = {"skymaps":("Sky maps",["_Significance1", "Excess1","ExcludedSignificance1",]),
      "totaldists":("Total distributions",["SignificanceDist1","ThetaSqr","LiveTime_Vs_CumulSignificance_CumuNorm","AlphaOff_Vs_CumulSignificance_CumuNorm"]),
      "runwise":("Runwise plots",["RunNumber_Vs_Excess", "RunNumber_Vs_CumulSignificance", "RunNumber_Vs_LiveTime", 
                 "RunNumber_Distribution",])
   }
   
   doc.add_heading("Plots")
   for plottype in plot_types:
      print(f"Adding {plottype}")
      title, plots = plot_names[plottype]
      
      doc.add_heading(title,level=2)
      tbl = doc.add_table(rows=1, cols=len(plots))
      row_cells = tbl.rows[0].cells
      for idx,plot in enumerate(plots):
        thisplot = [itm for itm in pngs if plot in itm][0]
        paragraph = row_cells[idx].paragraphs[0]
        #doc.add_picture(plot)
        print(f"Processing {thisplot}...",end="")
        par_run = paragraph.add_run()
        if os.path.exists(thisplot):
           par_run.add_picture(f"{paths.res_dir}/{thisplot}", width = 1400000)
           print("added")


   return doc

def add_stat_tables(paths,doc,stats):
    onoff= { "mean azimuth":['mean_azimuth_off', 'mean_azimuth_on'],
         "mean offset":['mean_offset_off', 'mean_offset_on'],
         "mean zenith":['mean_zenith_off', 'mean_zenith_on'],
         "num bad":['num_bad_off', 'num_bad_on'],
         "num":['num_off', 'num_on'],
         "num skipped":[ 'num_skipped_off', 'num_skipped_on',]} 

    rate = { "start_time":dt.datetime.utcfromtimestamp,
             "end_time":dt.datetime.utcfromtimestamp,
             "duration":lambda x: f"{x/3600:.2f} h",
             "events_per_minute":"{:.4f}".format,
             "live_time":lambda x: f"{x/3600:.2f} h",
             "live_fraction":"{:.4f}".format}

    event = {
          "significance":"{:.6f}".format,
          "excess": "{:.6f}".format,
          "alpha": "{:.6f}".format,
          "n_off": str,
          "n_on": str,
          "exposure_off": "{:.6f}".format,
          "exposure_on": "{:.6f}".format,
           }

    for stat in stats:
        with open(f"{paths.res_dir}/{stat}","r") as fil:
            data = json.load(fil)
        doc.add_heading(stat.split(".")[0])

        doc.add_heading("Events",level=2)
        events = data["rate_stats"]["event_stats"]
        table = doc.add_table(rows=len(event.keys()),cols=2)
        for idx,val in enumerate(event.keys()):
            row = table.rows[idx]
            frmt = event[val]
            print(val,frmt(events[val]))
            row.cells[0].text = val
            row.cells[1].text = str(frmt(events[val]))

        doc.add_heading("On Off stats",level=2)
        table2 = doc.add_table(rows=len(onoff.keys())+1,cols=3)
        row = table2.rows[0]
        row.cells[0].text = ""
        row.cells[1].text = "On"
        row.cells[2].text = "Off"
        for idx,val in enumerate(onoff.keys()):

            off_name,on_name = onoff[val]
            on,off = data[on_name],data[off_name]
            if isinstance(on,float):
                print(f"{val:<12}: {on: 9.4f} {off: 9.4f}")
            elif isinstance(on,int):
                print(f"{val:<12}: {on: 9d} {off: 9d}")

            row = table2.rows[idx+1]
            row.cells[0].text = val
            if isinstance(on,float):
                row.cells[1].text = f"{on:.4f}"
                row.cells[2].text = f"{off:.4f}"
            elif isinstance(on,int):
                row.cells[1].text = f"{on:d}"
                row.cells[2].text = f"{off:d}"
        
        doc.add_heading("Rate stats",level=2)
        rates = data["rate_stats"]
        table3 = doc.add_table(rows=len(rate.keys()),cols=2)
        for idx,val in enumerate(rate.keys()):
            row = table3.rows[idx]
            frmt = rate[val]
            print(val,frmt(rates[val]))
            row.cells[0].text = val
            row.cells[1].text = str(frmt(rates[val]))

        doc.add_page_break()

    return doc

def add_spectrum(paths,doc,pngs,stats):
   stat = next(stats)
   with open(f"{paths.res_dir}/{stat}","r") as fil:
       data = json.load(fil)

   nfits = data["fit_functions"]
   fits = []
   doc.add_heading("Spectrum")
   diag = ['Diagnostics', 'EnergyResolution']
   flux = [itm for itm in pngs if "Flux_" in itm]
   diagnostics = [itm for itm in pngs if diag[0] in itm or diag[1] in itm]
   for plot in flux:
       doc.add_picture(f"{paths.res_dir}/{plot}")

   for func in data["fit_functions"]["fit_functions"]:
       norm_scale = func["norm_scale"]
       doc.add_heading(func["type"],level=2)
       par_table = doc.add_table(
           rows = len(func["parameters"])+4,
           cols = 3)
       for idx,itm in enumerate(func["parameters"]):
           if itm["name"] == "Norm":
               value = itm["value"]*norm_scale
               error = itm["error"]*norm_scale
           else:
               value = itm["value"]
               error = itm["error"]
           row = par_table.rows[idx]
           row.cells[0].text = itm['name']
           row.cells[1].text = f"{value:.5e}"
           row.cells[2].text = f"{error:.5e}"
 
       row = par_table.rows[-4]
       row.cells[0].text = "Energy range"
       row.cells[1].text = f"{func['energy_min']:.4e}"
       row.cells[2].text = f"{func['energy_max']:.2e}"
       row = par_table.rows[-3]
       row.cells[0].text = "Flux at 1 TeV"
       row.cells[1].text = f"{func['flux_at_1']:.5e}"
       row.cells[2].text = f"{func['flux_at_1_err']:.5e}"
       row = par_table.rows[-2]
       row.cells[0].text = "Flux above 1 TeV"
       row.cells[1].text = f"{func['flux_above_1']:.5e}"
       row.cells[2].text = f"{func['flux_above_1_err']:.5e}"
       row = par_table.rows[-1]
       row.cells[0].text = "Energy range TeV"
       row.cells[1].text = f"{func['energy_min']:.4e}"
       row.cells[2].text = f"{func['energy_max']:.4e}"

       doc.add_heading("Fit statistics",level=3)
       fit_table = doc.add_table(
           rows = len(func["fit"].keys()),
           cols = 2)
       for idx,key in enumerate(func["fit"].keys()):
           row = fit_table.rows[idx]
           row.cells[0].text = key
           row.cells[1].text = str(func['fit'][key])

   doc.add_heading("Diagnostics",level=3)
   tbl = doc.add_table(rows=1, cols=2)
   row_cells = tbl.rows[0].cells
   count = 0
   for plot in diagnostics:
      if "Diagnostics" in plot:
         paragraph = row_cells[count].paragraphs[0]
         count += 1
         par_run = paragraph.add_run()
         if os.path.exists(plot):
            par_run.add_picture(f"{paths.res_dir}/{plot}", width = 1400000)
      else:
         doc.add_picture(f"{paths.res_dir}/{plot}")

   """# Try puttting it in a table
          # Stupid Confluence/Word resizes pictures while importing
          # so they become too large for confluence >:(
          resize_command = f"convert {paths.res_dir}/{plot} -resize 60% {paths.res_dir}/{plot}" 
          if "Diagnostics" in plot:
              try:
                  print("Resizing diagnostic plots")
                  result = sub.run(resize_command,
                      shell=True,stdout=sub.PIPE,check=True,executable="/bin/bash")
              except sub.CalledProcessError as err:
                  print('FAILED running convert:', err)
                  raise ValueError("Could not resize plot")
          doc.add_picture(f"{paths.res_dir}/{plot}")
   """
   return doc

def add_variability_section(paths,doc):
   lc_dir = f"{paths.res_dir[:-13]}/lightcurve_dir/"
   if os.path.exists(lc_dir):
      files = os.listdir(lc_dir)
   else:
      print("No variability plots found")
      return doc

   pngs  = list(filter(lambda s: "png" in s,files))
   if len(pngs) == 0:
      return doc

   plot_types = ["lightcurve", "nightly", "runwise"]

   plot_names = {"lightcurve":("Light curves",["LightCurves_",]),
      "nightly":("Nightly variability test",["ONOFF_Map_Nightly","ONOFF_MaxPos_Nightly","ONOFF_TestPos_Nightly"]),
      "runwise":("Runwise variability test",["ONOFF_Map_run","ONOFF_MaxPos_run","ONOFF_TestPos_run"]),
   }

   doc.add_heading("Variability")
   for plottype in plot_types:
      print(f"Adding {plottype}")
      title, plots = plot_names[plottype]

      doc.add_heading(title,level=2)
      tbl = doc.add_table(rows=1, cols=len(plots))
      row_cells = tbl.rows[0].cells
      for idx,plot in enumerate(plots):
        try:
           thisplot = [itm for itm in pngs if plot in itm][0]
        except:
            print(f"WARNING: Plot {plot} not found, I suggest you look at the runlog for hints about why it is missing.")
            continue
        paragraph = row_cells[idx].paragraphs[0]
        plotpath = f"{lc_dir}/{thisplot}"
        print(f"Processing {plotpath}...",end="")
        par_run = paragraph.add_run()
        if os.path.exists(plotpath):
           par_run.add_picture(plotpath, width = 1400000)
           print("added")


   return doc

def make_report(config,name,res_dir):

    filename = f"{name}_merge.root"
    paths = Paths(file=f"{res_dir}/{filename}",
                  src_filename=filename,
                  res_dir=res_dir, )
    
    doc = dx.Document()
    files = os.listdir(paths.res_dir)
    pngs  = list(filter(lambda s: "png" in s,files))

    doc = add_map_plots(paths,doc,pngs)
    doc = add_variability_section(paths,doc)

    stats = filter(lambda s: "stats.json" in s,files)
    doc = add_stat_tables(paths,doc,stats)

    stats = filter(lambda s: "spectrum.json" in s,files)
    doc = add_spectrum(paths,doc,pngs,stats)    

    return doc

def make_fitspectrum_config(config,name,res_dir):
    conf = """
# Input Output
OutputJSONFile = {out_file}
InputFile = {in_file}
# Fit Settings
Emin = {emin}
Emax = {emax}
FitAlgo = {algo}
FitModels = {models}
PlotOption = {plotop}
BatchMode = True
Extension = .png
"""
    conf_file = "{res_dir}/FitSpectrum_{name}_{config}.conf".format(
        res_dir=res_dir,
        name=name,
        config=config["HapConfig"])
    json_file =  "{res_dir}/{name}_{config}_spectrum.json".format(
        res_dir=res_dir,
        name=name,
        config=config["HapConfig"])
    in_file= "{res_dir}/{src_name}_merged.root".format(
        res_dir=res_dir,
        src_name=name)

    conf = conf.format(
        out_file=json_file,
        in_file=in_file,
        emin=config["Emin"],
        emax=config["Emax"],
        algo=config["FitAlgo"],
        models=config["FitModels"],
        plotop=config["PlotOption"],
 )
        
    return conf_file, conf
