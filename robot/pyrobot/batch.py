import pyrobot
import pyrobot.config_handler as cf
import pyrobot.utils as ut

import configparser
import os
import stat

def make_batch_files_from_config(sources_config):
   ut.make_dir(sources_config["DEFAULT"]["SubmitScriptDir"])
   for src in sources_config.sections():
      source = sources_config[src]
      script = make_submission_script(source)
      filename = f"{source['SubmitScriptDir']}/submit_{source['name']}.sh"
      with open(filename,"w+") as fil:
         print(f"Writing {filename}")
         fil.writelines(script)
         mode = os.stat(filename).st_mode
         os.chmod(filename, mode | stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH)

def make_config(case,source):
   source_config = cf.make_HAP_source_string(source,case)
   runlist = f'{source["RunListDir"]}/{case["list"]}'
   rootdir = f'{source.get("ProjRootDir",fallback=".")}/{source["name"]}/'
   ut.make_dir(rootdir)
   source_config = source_config.format(
      runlist=ut.fullpath(runlist),
      era=case["era"],
      rootdir=ut.fullpath(rootdir) )
   # Use cat and recirect to ensure
   # the saving works in non-interactive
   # shells too (those without tty)
   config = """
{conf_case}=$(cat <<'EOF'
{heredoc}
EOF
)

""".format(conf_case= case['era'],
           heredoc = source_config)

   return config

def make_execution_statement(case,source):
   conf_name = f"{case['era']}_{source['name']}"
   if case["era"] in source.get("DstScheme",fallback="-"):
      dst = pyrobot.HAP_PATHS[source["DstScheme"]]["dst"]
      data = pyrobot.HAP_PATHS[source["DstScheme"]]["data"]
   else:
      dst = pyrobot.HAP_PATHS[case["era"]]["dst"]
      data = pyrobot.HAP_PATHS[case["era"]]["data"]


   config_file_path = "{}/{}.conf".format(
       source["SubmitScriptDir"],
       conf_name)

   execute_string = """
if [ -e "{config_file}" ]; then
rm "{config_file}"
fi
printf "${era}" > "{config_file}"

export HESSCONFIG=/lfs/l7/hess/analysis/version36
export HESSDST={dst}
export HESSDATA={data}

printf "Running $HESSROOT/hddst/scripts/hap_split.pl --include  {config_file}"
$HESSROOT/hddst/scripts/hap_split.pl --include  "{config_file}"
""".format(config_file=ut.fullpath(config_file_path),
           era=case["era"],
           dst=dst,
           data=data)

   return execute_string


def make_submission_script(source):
   runlist = f"{source['RunListDir']}/{source['RunListFile']}"
   if os.path.exists(runlist):
      cases = ut.split_list(runlist,
                            source["name"],
                            source['RunListDir'])
   else:
      raise ValueError(f"Runlist {runlist} not found")

   script = """
source ~hess/{script}
now=$(date -I'minutes')
printf 'Running submission {name} at %s\\n' "$now"
exec &> >(tee -a "submission_{name}-$now")

""".format(script = pyrobot.HAP_DEFAULT["hap-setup-script"],
           name = source["name"])

   for case in cases:
      script += make_config(case,source)

   for case in cases:
      script += make_execution_statement(case,source)

   return script
