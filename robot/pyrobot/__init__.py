HESS1_END = 83490
HESS2_END = 127699

HAP_CONF_STR = """
[OPTIONS]
  config = {config}
  runlist = {runlist_name}
  outdir = {outdir_name}
  outfile = {outfile_name}
  verbose = true

[Background]
  Method = ReflectedBg   # for spectrum
  ExtraMethods = RingBg  # for maps
  SafeEnergyRangeOnly = true  # fill maps & stats only in safe range
  TestPosRA = {RA}         # if not to be read from the DST
  TestPosDec = {DEC}       # if not to be read from the DST
  FOV = 4.0                   # size of resulting maps in degrees

  ExclusionRegionsFile = {exclusion_file} 

  AllowedEnergyBias = 0.1

[Analysis]
  AllowedTels = {tels}

[Spectrum]
  Method = Standard
  AllowedEnergyBias = 0.1

[Diagnostics]
  WriteEventTree = true
  ListOfSkippedVariables = ShowerImage
  DiagnosticFolder = ReflectedBgMaker_On
  SecondaryFolder = ReflectedBgMaker_Off
"""

FILTER_BEFORE_HAP_CONFIG = [
"Emin",
"Emax",
"telpat_kind",
"DstScheme",
"name",
]

HAP_DEFAULT = {
    "hap-setup-script":"hap-18-11.sh",
    "progress" : "false", 
    "verbose" : "true",
    "BgMethod" :"ReflectedBg",
    "BgExtraMethod":"RingBg",
    "AcceptanceFromData" : "false",
    "BgAllowedEnergyBias" : "0.1",
    "BgFOV":"4.0",
    "SpMethod" : "Standard",
    "SpAllowedEnergyBias" : "0.1",
    "WriteEventTree" : "true",
    "DiagnosticFolder" : "ReflectedBgMaker_On",
    "SecondaryFolder" : "ReflectedBgMaker_Off",
    "LcGenerate" : "true", 
    "tels": "1,2,3,4"
}

HAP_PATHS={
    "hess1":{
        "dst":"/lfs/l2/hess/dstdata_extended_13-06",
        "data":"/lfs/l2/hess/hddata/"},
    "hess2":{
        "dst":"/lfs/l2/hess/users/marandon/CalibData/CT5/dst_WithSimpleGainMerge_NewBrkCard_AllDT_extended2B_FallbackCalib",
        "data":"/lfs/l7/hess/users/marandon/CalibData/CT5/hddata_WithSimpleGainMerge_NewBrkCard2/"},
    "hess1u":{
        "dst":"/lfs/l7/hess/users/marandon/CalibData/CT5/dst_NewScheme",
        "data":"/lfs/l7/hess/users/marandon/CalibData/CT5/hddata_NewScheme/"},
    "hess1u_SimpleGainMerge":{
        "dst":"/lfs/l2/hess/users/marandon/CalibData/CT5/dst_WithSimpleGainMerge_NewBrkCard_AllDT_extended2B_FallbackCalib",
        "data":"/lfs/l7/hess/users/marandon/CalibData/CT5/hddata_WithSimpleGainMerge_NewBrkCard2/"},
    "hess2_NewScheme":{
        "dst":"/lfs/l7/hess/users/marandon/CalibData/CT5/dst_NewScheme",
        "data":"/lfs/l7/hess/users/marandon/CalibData/CT5/hddata_NewScheme/"},
    "hess1_NewScheme2":{
        "dst":"/lfs/l7/hess/users/marandon/CalibData/CT5/dst_NewScheme2",
        "data":"/lfs/l7/hess/users/marandon/CalibData/CT5/hddata_NewScheme2"},
}

