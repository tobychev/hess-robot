import glob
import os
import pyrobot
import re


## Fancy sorting to ensure hess1 hess2 hess1u order
# https://stackoverflow.com/questions/26579392/
HESSERAS="_12u"

# From https://stackoverflow.com/questions/5967500
def atoi(text):
    return int(text) if text.isdigit() else text

def natural_keys(text,reg):
    '''
    alist.sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    (See Toothy's implementation in the comments)
    '''
    return [ atoi(c) for c in re.split(reg, text) ]

def haplogs_natural_keys(text):
    return natural_keys(text,r'_(\d+).out')

def hapnames_natural_keys(text):
    return natural_keys(text,r'_(\d+).hap')

def event_natural_keys(text):
    return natural_keys(text,r'_(\d+).root')

def mangle_source_name(name):
    return name.replace(" ","_").replace(".","o").replace("+","-")

def unmangle_name(name,config):
   return list(filter(lambda x: mangle_source_name(x) == name, config.sections())).pop()

def fullpath(path):
    return os.path.abspath(
        os.path.expanduser(path))

def make_dir(folder):
    folder = fullpath(folder)
    if not os.path.isdir(folder):
        print(f"Creating {folder}")
        os.mkdir(folder)
    return folder

def split_list(runlist,base_name,runlistdir):

   with open(runlist,"r") as fil:
      runlines = fil.readlines()
   hess1 = []
   hess2 = []
   hess1u = []
   for runline in runlines:
      run = int(runline.split()[0])
      if run <= pyrobot.HESS1_END:
         hess1.append(runline)
      elif run <= pyrobot.HESS2_END:
         hess2.append(runline)
      else:
         hess1u.append(runline)

   configs = []
   if len(hess1) > 0:
      with open(f"{runlistdir}/{base_name}_hess1_runs.list","w+") as fil:
         fil.writelines(hess1)
      configs.append({"era":"hess1","list":base_name + "_hess1_runs.list"})
   if len(hess2) > 0:
      with open(f"{runlistdir}/{base_name}_hess2_runs.list","w+") as fil:
         fil.writelines(hess2)
      configs.append({"era":"hess2","list":base_name + "_hess2_runs.list"})
   if len(hess1u) > 0:
      with open(f"{runlistdir}/{base_name}_hess1u_runs.list","w+") as fil:
         fil.writelines(hess1u)
      configs.append({"era":"hess1u","list":base_name + "_hess1u_runs.list"})

   return configs

def list_hap_files_to_merge(srcdir,srcname):
   paths = []
   for fil in os.listdir(srcdir):
      path = f"{srcdir}/{fil}"
      if ((fil.split("_")[0] in ["hess1","hess1u","hess2"]) 
            and os.path.isdir(path)):
         #TODO: This assumes only one analysis result per era ever exists in these dirs,
         #      can probably be made more robust
         result = [test for test in glob.glob(f"{path}/{srcname}*") if os.path.isdir(test)][0]
         paths.append(result)

   files = []
   for path in paths:
      candidates = glob.glob(f"{path}/*.hap/*.root")
      cleaned = [fil for fil in candidates if not "Lightcurve" in fil]
      cleaned.sort(key=hapnames_natural_keys)
      files += cleaned
   return ",".join(files)

def symlink_resultfiles_to_directory(hap_config,src_name,src_dir,target_dir):
   pattern = f"hess*_{src_name}_{hap_config}"
   dirs = glob.glob(f"{src_dir}/{pattern}")
   dirs = sorted(dirs, key=lambda word: [HESSERAS.index(c) for c in word.split("/")[-1][4:6]])
   event_files = []
   hap_dirs = []
   for loc in dirs:
      eventfiles = glob.glob(f"{loc}/{src_name}_{hap_config}/events_*.root")
      eventfiles.sort(key=event_natural_keys)
      event_files += eventfiles
      hapdirs = glob.glob(f"{loc}/{src_name}_{hap_config}/{src_name}_{hap_config}_*.hap")  
      hapdirs.sort(key=hapnames_natural_keys)
      hap_dirs += hapdirs

   root_files = []
   for loc in hap_dirs:
      root_files += glob.glob(f"{loc}/{src_name}_*_{hap_config}.root")

   for idx,fil in enumerate(event_files):
      newfile = f"{target_dir}/events_{idx}.root"
      if not os.path.exists(newfile):
         os.symlink(fil,newfile)


   for idx,fil in enumerate(root_files):
      # Format required by START
      newfile = f"{target_dir}/run_{idx}_{hap_config}.root" 
      if not os.path.exists(newfile):
         os.symlink(fil,newfile)
   return len(root_files)

def check_run_results(src_name,config):
   hap_config = config["HapConfig"]
   src_dir = f"{config['ProjRootDir']}/{src_name}"
   dirs = glob.glob(f"{src_dir}/hess*_{src_name}_{hap_config}")
   dirs = sorted(dirs, key=lambda word: [HESSERAS.index(c) for c in word.split("/")[-1][4:6]])
   messages = {}
   errors = 0 
   for loc in dirs:
      logs = glob.glob(f"{loc}/{src_name}_{hap_config}/hap_*.out")
      logs.sort(key=haplogs_natural_keys)
      message = [loc.split("/")[-1]+"\n"]
      for log in logs:
         with open(log,"r") as fil:
            lines = fil.read()
            if lines.find("HAP finished.") >= 0:
               continue

            if lines.find("There were 1 runs missing DSTs") >= 0:
               message.append(f"Error in file: {log}\n")
               fil.seek(0)
               for line in fil:
                  if "There were 1 runs missing DSTs" in line:
                     message.append(line) 
                     line = fil.readline()
                     message.append(line+"\n") 
                     break
               errors += 1
               continue

            if lines.find("Failed to find Primary Dataset") >= 0:
               message.append(f"Error in file: {log}\n")
               fil.seek(0)
               for line in fil:
                  if "Failed to find Primary Dataset" in line:
                     message.append(line) 
                     break
               errors += 1
               continue

            message.append(f"Unknown error in file: {log}\n")

      message.append(errors)
      if len(message) > 2:
         messages[message[0][:-1]] = message
   

   if len(messages) > 0:
      keys = messages.keys()
      errors = 0
      msg = ""
      for key in keys:
         errors += messages[key].pop()   
      
      for key in keys:
         msg +=  "".join(messages[key])+"\n"
      return errors, msg

   else:
      return 0,""
