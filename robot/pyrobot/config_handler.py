import pyrobot
import pyrobot.utils as ut

import configparser
import io
import os

def parse_sources_config(filename):
   conf = configparser.ConfigParser(allow_no_value=True)
   conf.optionxform=str
   conf.read(filename)
   rootdir = conf.get("DEFAULT","ProjRootDir",fallback=".")
   conf.set("DEFAULT","ProjRootDir",ut.fullpath(rootdir))

   for sec in conf.sections():
      conf[sec]["name"] = ut.mangle_source_name(sec)

   test_source_sections(conf)

   return conf

def make_HAP_source(source,case):
   src_conf = configparser.ConfigParser()
   src_conf.optionxform=str
   src_conf["OPTIONS"] = {
      'config'  : source["HapConfig"],
      'runlist'  : "{runlist}",
      'outfile' : "{}_{}".format(source["name"],source["HapConfig"]),
      'outdir'  : '{rootdir}/{era}'+'_{}_{}'.format(source["name"],source["HapConfig"]),
      'progress' : source.get("progress",
                              fallback=pyrobot.HAP_DEFAULT["progress"]),
      'verbose' : source.get("verbose",
                             fallback=pyrobot.HAP_DEFAULT["verbose"])
   }

   src_conf["Background"] = {
      'System': source["PosSystem"],
      'TestPosRA' : source["Ra"],
      'TestPosDec' : source["Dec"],
      'ExclusionRegionsFile' : ut.fullpath(source["ExclusionFile"]),
      'Method' : source.get("BgMethod",
                            fallback=pyrobot.HAP_DEFAULT["BgMethod"]),
      'ExtraMethods' : source.get("BgExtraMethod",
                                  fallback=pyrobot.HAP_DEFAULT["BgExtraMethod"]),
      'AcceptanceFromData' : "",
      'AllowedEnergyBias' : source.get("BgAllowedEnergyBias",
                                       fallback=pyrobot.HAP_DEFAULT["BgAllowedEnergyBias"]),

   }
   if case["era"] == "hess1" and source["HapConfig"] == "loose_ImPACT":
      src_conf["Background"]["AcceptanceFromData"] = "True"
   elif case["era"] == "hess2" and source["HapConfig"] == "loose_ImPACT":
      src_conf["Background"]["AcceptanceFromData"] = "True"
   elif case["era"] == "hess2" and source["HapConfig"] == "loose_ImPACT_hybrid":
      src_conf["Background"]["AcceptanceFromData"] = "True"
   elif case["era"] == "hess1u" and source["HapConfig"] == "loose_ImPACT_hybrid":
      src_conf["Background"]["AcceptanceFromData"] = "True"
   else:
      src_conf["Background"]["AcceptanceFromData"] = source.get("AcceptanceFromData",
            fallback=pyrobot.HAP_DEFAULT["AcceptanceFromData"])

   if source.get("fovx",fallback=False):
      src_conf.set("Background","FOV", f'{source["fovx"]},{source["fovy"]}')
   else:
      src_conf.set("Background","FOV", source.get("BgFOV",
                                                fallback=pyrobot.HAP_DEFAULT["BgFOV"]))
      
   src_conf["Analysis"] = {
      'AllowedTels' : source.get("tels",
                        fallback=pyrobot.HAP_DEFAULT["tels"]),
   }

   src_conf["Spectrum"] = {
      'Method' : source.get("SpMethod",
                        fallback=pyrobot.HAP_DEFAULT["SpMethod"]),
      'AllowedEnergyBias' : source.get("SpAllowedEnergyBias",
                        fallback=pyrobot.HAP_DEFAULT["SpAllowedEnergyBias"]),
   }

   src_conf["Diagnostics"] = {
      'WriteEventTree' : source.get("WriteEventTree",
                        fallback=pyrobot.HAP_DEFAULT["WriteEventTree"]),
      'DiagnosticFolder' : source.get("DiagnosticFolder",
                        fallback=pyrobot.HAP_DEFAULT["DiagnosticFolder"]),
      'SecondaryFolder' : source.get("SecondaryFolder",
                        fallback=pyrobot.HAP_DEFAULT["SecondaryFolder"]),
   }

   src_conf["Lightcurve"] = {
      'Generate' : source.get("LcGenerate",
                        fallback=pyrobot.HAP_DEFAULT["LcGenerate"]),
      'LightcurveName' : 'Lightcurve ',
   }
   return src_conf

def make_HAP_source_string(source_config,case):
   src_conf = make_HAP_source(source_config,case)
   with io.StringIO("") as tmpfile:
      src_conf.write(tmpfile)
      source_string = tmpfile.getvalue()

   return source_string

def make_HAP_sources_strings(sources_config,case):
   sources = []
   for src in sources_config.sections():
      source = sources_config[src]
      src_conf = make_HAP_source(source,case)
      with io.StringIO("") as tmpfile:
         src_conf.write(tmpfile)
         sources.append(tmpfile.getvalue())

   return sources

def test_source_sections(sources_config):
   for src in sources_config.sections():
      if not ( sources_config.has_option(src,"Ra") &
              sources_config.has_option(src,"Dec") &
              sources_config.has_option(src,"RunListFile")):

         raise ValueError(f"Source {src} does not have target ra, dec or RunListFile")
