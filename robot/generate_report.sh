#/usr/bin/env bash
if test "$BASH" = "" || "$BASH" -uc "a=();true \"\${a[@]}\"" 2>/dev/null; then
    # Bash 4.4, Zsh
    set -euo pipefail
else
    # Bash 4.3 and older chokes on empty arrays with set -u.
    set -eo pipefail
fi
shopt -s nullglob globstar

# -allow a command to fail with !’s side effect on errexit
# -use return value from ${PIPESTATUS[0]}, because ! hosed $?
! getopt --test > /dev/null 
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
    echo 'I’m sorry, `getopt --test` failed in this environment.'
    exit 1
fi


enablepython()
{
if ! python -c 'import sys; assert sys.version_info >= (3,6)' 2> /dev/null; then
	if [ "$HOSTNAME" == "lfs1.mpi-hd.mpg.de" ]; then
		export  LD_LIBRARY_PATH=/home/hfm/hess/software/root/latest/lib:/nfs/d22/hfm/hess/Base_Software_Linux64_gcc4.8a/usr/lib64:/opt/gnome/lib:/nfs/d22/hfm/hess/Base_Software_Linux64_gcc4.8a/usr/lib64/root:/home/hfm/hess/hap-18-11//lib:/usr/lib64/:/usr/lib64/mysql/
		export LIBPATH=/home/hfm/hess/software/root/latest/lib
		export PATH=/home/hfm/hess/software/root/latest/bin:/home/hfm/hess/software/python/miniconda/miniconda/bin:/home/hfm/hess/hap-18-11//scons/local/:/home/hfm/hess/hap-18-11//bin:/nfs/d22/hfm/hess/Base_Software_Linux64_gcc4.8a/usr/bin:/nfs/d22/hfm/hess/Base_Software_Linux64_gcc4.8a/bin:.:/home/extern/tobychev/bin:/usr/local/bin:/bin:/usr/bin:/opt/gnome/bin:/usr/bin/X11:/home/hfm/hess/hap-18-11//hddst/bin:/usr/local/sbin:/usr/sbin:/home/extern/tobychev/bin
		export PYTHONPATH=/home/hfm/hess/software/root/latest/lib:/nfs/d22/hfm/hess/Base_Software_Linux64_gcc4.8a/usr/bin/../root/lib:/home/hfm/hess/hap-18-11//dash:/home/hfm/hess/hap-18-11//hesspy
	fi
	if [ "$HOSTNAME" == "lb-00009" ]; then
		conda activate base
	fi
fi
}

usage ()
{
    echo "usage: generate_report.sh sources.ini [-d] [options]"
    echo "  First argiment must be the source ini file"
    echo "  Internal tool, not really for manual use "
	 echo "  -d                       debug"
	 echo "  --loc <path>             location of the hess-robot root directory"
	 echo "  --srcdir <path>          location of the HAP output results for this source"
	 echo "  --srcname <name>         name of this source"
    exit 1	
}

enablepython

STARTDIR="$(pwd)"


#  First thing given must be the source ini file
SRCINI="$1"
shift

LONGOPTS=debug,loc:,srcdir:,srcname:

# -regarding ! and PIPESTATUS see above
# -temporarily store output to be able to check for errors
# -activate quoting/enhanced mode (e.g. by writing out “--options”)
# -pass arguments only via   -- "$@"   to separate them correctly
! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # e.g. return value is 1
    #  then getopt has complained about wrong arguments to stdout
    exit 2
fi
# read getopt’s output this way to handle the quoting right:
eval set -- "$PARSED"

debug=n dir=n name=n
srcDir=- srcName=- robotdir=- 
# now enjoy the options in order and nicely split until we see --

while true; do
    case "$1" in
        --debug)
            debug=y
            shift
            ;;
        --loc)
            robotdir="$2"
            shift 2
            ;;
        --srcdir)
				dir=y
            srcDir="$2"
            shift 2
            ;;
        --srcname)
            name=y
            srcName="$2"
            shift 2
            ;;
        --)
            shift
				if [ ${#PARSED} -eq 3 ]; then
					usage
				fi
				if [ ! -z "${1+x}" ]; then
					echo "Unknown option $1"
					usage
				fi
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

if [ "$debug" == y ]; then
   printf "\n"
   printf "Generate report arguments: \n"
   printf "%s\n" "$PARSED"
   printf "Src name %s %s\n" $name "$srcName"
   printf "Srcdir %s %s\n" $dir  "$srcDir"
   printf "Loc %s\n" "$robotdir"
fi


# Ensure there is a merged result file in the correct place
# and that there is directory to put all intermediary results

# The reason for creating the link file it to keep one testing logic
# for both reporting on one era analysis (hess1,hess1u,hess2) and the
# complete set spanning several eras
if  ! (compgen -G "$srcDir/${srcName}_*_merged.root" > /dev/null); then
   if [ "$debug" == y ]; then
      printf "Merge file %s/%s_*_merged.root not found, merging\n" "$srcDir" "${srcName}"
   fi 

	files=$(python $robotdir/robot/hess_robot.py "dummy" --generate-hap-merge-list "$srcDir" "$srcName")
   mergefile=$(printf "%s/report_files/%s_merged.root" "$srcDir" "$srcName")
   source ~hess/hap-18-11.sh > /dev/null
   
   if [ ! -d $srcDir/report_files ]; then
      if [ "$debug" == y ]; then
         printf "mkdir %s/report_files" "$srcDir"
      fi
      mkdir $srcDir/report_files
   fi
   if [ "$debug" == y ]; then
      printf "hap-merge --init-from-first --system RADEC --dataset RunResult --copy-input --verbose true --output %s --files %s" "$mergefile" "${files}"
   fi
   hap-merge --init-from-first --system RADEC --dataset RunResult --copy-input --verbose true --output "$mergefile" --files $files

   cd "$srcDir"
   ln -s "$mergefile" "${srcName}_link_merged.root" 
   
   # Sets paths so correct python version is used downscript
   enablepython

else
   if [ ! -d $srcDir/report_files ]; then
      if [ "$debug" == y ]; then
         printf "mkdir %s/report_files" "$srcDir"
      fi
      mkdir "$srcDir/report_files"
   fi
   if [ ! -e "$srcDir/report_files/${srcName}_merged.root" ]; then
      mergefile=$(compgen -G "$srcDir/${srcName}_*_merged.root")
      mergefile=$(basename $mergefile)
      if [ "$debug" == y ]; then
         printf  "Copying merge file:\ncp %s/%s %s/report_files\n" "$srcDir" "$mergefile" "$srcDir"
      fi
      cp "$srcDir/$mergefile" "$srcDir/report_files/${srcName}_merged.root"
   fi
fi

if [  -d $srcDir/report_files ]; then
   if [ "$debug" == y ]; then
	   echo "Copying root scripts to the report directory $srcDir/report_files"
   fi
   cp $robotdir/robot/rootrobot/DrawRunResults.C $robotdir/robot/rootrobot/GenerateRunResults.C "$srcDir/report_files" 

   if [ "$debug" == y ]; then
	   echo "Generating config for fitspectrum using $SRCINI, then runnnig fitspectrum"
	   echo "python $robotdir/robot/hess_robot.py $SRCINI --generate-fitspectrum-config $srcName $srcDir/report_files"
	   printf "%s" "$(python $robotdir/robot/hess_robot.py "$SRCINI" --generate-fitspectrum-config "$srcName" "$srcDir/report_files")"
   fi
	conf_file=$(python $robotdir/robot/hess_robot.py "$SRCINI" --generate-fitspectrum-config "$srcName" "$srcDir/report_files")
   echo $conf_file
   conf_file=$(echo "$conf_file" | tail -n1) # Get last line of the output because that's the name of the conf file

   cd $srcDir/report_files

   FitSpectrum --include "$conf_file" > "result_fitspectrum_$(date -I'minutes').log" 
    
   if [ "$debug" == y ]; then
	   echo "Runing the root diagnositic scripts $srcDir/report_files"
   fi
   root -b -q "GenerateRunResults.C(\"${srcName}_merged.root\",\"$srcName\")" > results_Run_$(date -I'minutes').log 

   if [ "$debug" == y ]; then
	   echo "Collecting results into report document"
   fi
   report_file=$(python $robotdir/robot/hess_robot.py "$STARTDIR/$SRCINI" --generate-report "$srcName" "$srcDir/report_files")
   report_file=$(echo "$report_file" | tail -n1) # Get last line of the output because that's the name of the report file
   cp "$report_file" "$STARTDIR"

   cd "$STARDIR"
else
   printf "No directory with results analyse found at %s" "$srcDir/report_files"
   exit -1
fi

exit 0
