#include <TFile.h>
#include <TMarker.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TSystem.h>
#include <TPad.h>
#include <TSystemDirectory.h>
#include <TSystemFile.h>
#include <TLegend.h>

#include <tools/Region.hh>

#include <sash/DataSet.hh>
#include <sash/HESSArray.hh>
#include <sash/Folder.hh>
#include <sash/RunHeader.hh>
#include <sash/EventSelection.hh>
#include <sash/PointerSetIterator.hh>
#include <sash/Telescope.hh>

#include <background/Utils.hh>
#include <background/BgMaps.hh>
#include <background/BgStats.hh>
#include <background/RingBgMaker.hh>

#include <plotters/SkyHist.hh>
#include <plotters/FITSTools.hh>
#include <plotters/Palette.hh>

#include <crash/RotatedSystem.hh>
#include <crash/EquatorSystem.hh>

#include <utilities/Statistics.hh>


/*
  This little script loops over RTA output files for a given configuration and 
  target, adds up the run results, and displays the total maps and stats
 */
void ProcessOutputFile(TString fname, Background::BgStats* fCurrentStats, 
             Background::BgMaps* fCurrentMaps);
double get_var_sigma(std::map<int,double> vDate_In,
           std::map<int,double> vAccs_In,
           std::map<int,double> vEvents_In,
           std::map<int,double> vAllowed_In,
           int& vecsize, bool fillhistos, std::vector<TH1F*> &histos,
           TString prefix);

double get_var_sigma(std::map<int,double> vDate_In,
           std::map<int,double> vAccs_In,
           std::map<int,double> vEvents_In,
           std::map<int,double> vAllowed_In,
           int& vecsize, bool fill_histos, std::vector<TH1F*> &histos,
           TString prefix)
{
 
  // DO THE TEST
  int iterations = 0;
  int changes = 1;
  std::map<int,double> vON, vOFF, vAlphaON, vAlphaOFF, vExcess, vSig;

  while(changes != 0){
    //std::cout << "Got In" << std::endl;
    int countchanges = 0;
    vON.clear();
    vOFF.clear();
    vAlphaON.clear();
    vAlphaOFF.clear(); 
    vExcess.clear(); 
    vSig.clear();
    
    std::map<int,double>::iterator itD = vDate_In.begin();
    std::map<int,double>::iterator itA = vAccs_In.begin();
    std::map<int,double>::iterator itE = vEvents_In.begin();
    std::map<int,double>::iterator itX = vAllowed_In.begin();
    
    for(; itD != vDate_In.end(); ++itD, ++itA, ++itE, ++itX){
    
      //      std::cout << itD->first << " " << itD->second << std::endl;
    
      double this_entry = itD->first;
      vON[itD->first]= itE->second;
      vAlphaON[itD->first]= itA->second;
      std::map<int,double>::iterator itDIn = vDate_In.begin();
      std::map<int,double>::iterator itAIn = vAccs_In.begin();
      std::map<int,double>::iterator itEIn = vEvents_In.begin();
      std::map<int,double>::iterator itXIn = vAllowed_In.begin();
      double sumAcc = 0;
      double sumEv = 0;
    
      for(; itDIn != vDate_In.end(); ++itDIn, ++itAIn, ++itEIn, ++itXIn){ // Looping over all the runs to create the bckg sample
   if(itDIn->first != this_entry && itXIn->second != 0){
     sumAcc += itAIn->second;
     sumEv += itEIn->second;
     // std::cout << "sumAcc " << sumAcc 
     //      << " sumEv " << sumEv << std::endl;
   }
      }
    
      vOFF[itD->first]= sumEv;  // Storing the background of each run in a map
      vAlphaOFF[itD->first]= sumAcc;
    }
  
    //
    std::map<int,double>::iterator itON = vON.begin();
    std::map<int,double>::iterator itOFF = vOFF.begin();
    std::map<int,double>::iterator itAON = vAlphaON.begin();
    std::map<int,double>::iterator itAOFF = vAlphaOFF.begin();
    std::map<int,double>::iterator itX2 = vAllowed_In.begin();

    for(; itON != vON.end(); ++itON, ++itOFF, ++itAON, ++itAOFF, ++itX2){
      double non = itON->second;
      double noff = itOFF->second;
      double alpha = itAON->second*1./itAOFF->second;
      double sig = Utilities::Statistics::Significance(non,noff,alpha);

      if(sig > 4.5 && itX2->second == 1){       //Checking the significance of the current run and removing it from bckg if needed
   // vAllowed_In[itX2->first-2] = 0;
   // vAllowed_In[itX2->first-1] = 0;
   vAllowed_In[itX2->first] = 0;
   // vAllowed_In[itX2->first+1] = 0;
   // vAllowed_In[itX2->first+2] = 0;
      
   ++countchanges;
      }
    }
    changes = countchanges;
    ++iterations;
  }

  // std::cout << "Size " << vON.size() << std::endl;
  vecsize = vON.size();
  std::map<int,double>::iterator itON = vON.begin();
  std::map<int,double>::iterator itOFF = vOFF.begin();
  std::map<int,double>::iterator itAON = vAlphaON.begin();
  std::map<int,double>::iterator itAOFF = vAlphaOFF.begin();
  std::map<int,double>::iterator itX2 = vAllowed_In.begin();  
  std::map<int,double>::iterator itONBegin = vON.begin();
  std::map<int,double>::iterator itONEnd = vON.end();

  double maxsig = 0;
  for(; itON != vON.end(); ++itON, ++itOFF, ++itAON, ++itAOFF, ++itX2) {
    double non = itON->second;
    double noff = itOFF->second;
    double alpha = itAON->second*1./itAOFF->second;
    double sig = Utilities::Statistics::Significance(non,noff,alpha);
    //    std::cout << non-alpha*noff << " " << sig  << std::endl;

    if(sig > maxsig)
      maxsig = sig;

  }

  itON = vON.begin();
  itOFF = vOFF.begin();
  itAON = vAlphaON.begin();
  itAOFF = vAlphaOFF.begin();
  itX2 = vAllowed_In.begin();

  // 
  if(fill_histos) {
    // initialize histos
    const char* n[7] = {"ON","AOFF","Excess","Signif","Allowed","SignifD","SignifDA"};
    for(int i = 0; i < 7; ++i) {
      TString name = prefix;
      name += n[i];
      histos[i]->SetNameTitle(name.Data(),name.Data());
      if(i<5) histos[i]->SetBins(vON.size(),-0.5,(itONEnd->first)-0.5);
      else histos[i]->SetBins(80,-10.,10.);
    }
    
    int count2 = 0;
    for(; itON != vON.end(); ++itON, ++itOFF, ++itAON, ++itAOFF, ++itX2){
    // for(int i = 0; i < vecsize; ++i) {
      // int this_entry = itON->first;
      double non = itON->second;
      double noff = itOFF->second;
      double alpha = itAON->second*1./itAOFF->second;
      double sig = Utilities::Statistics::Significance(non,noff,alpha);
      double allowed = itX2->second;
      
      histos[0]->Fill(count2, non);
      histos[1]->Fill(count2, alpha*noff);
      histos[2]->Fill(count2, non - alpha*noff);
      histos[3]->Fill(count2, sig);
      histos[4]->Fill(count2, allowed);
      histos[5]->Fill(sig);
      if(allowed == 1)
   histos[6]->Fill(sig);
      ++count2;
    }
  } // end filling of histos

  return maxsig;

}



//void make_transient_test( std::string storageDirectory = "/lustre/fs10/group/hess/user/bonnefoy/data/hap/hegs/cluster99/variability/outfiles",
void make_transient_test( std::string prefix = "Cluster27" ,
        std::string storageDirectory = "/home/hfm/sanchez/HEGS/HAP18/version_Oct2020/Var/std_ImPACT/Cluster27",
           std::string config = "std_ImPACT",
           int minrun = 10000, double theta = 0.1, 
           bool withCT5only = false)
{

  // Create empty maps and stats
  Sash::HESSArray* hess = &Sash::HESSArray::GetHESSArray();
  Background::BgStats *fCurrentStats = 
    hess->Handle<Background::BgStats>("RingBgMaker_Current");
  Background::BgMaps *fCurrentMaps = 
    hess->Handle<Background::BgMaps>("RingBgMaker_Current");

  Background::BgStats *fTotalStats = 
    hess->Handle<Background::BgStats>("RingBgMaker_Total");
  Background::BgMaps *fTotalMaps = 
    hess->Handle<Background::BgMaps>("RingBgMaker_Total");

  // Load list of files
  std::stringstream resultFileName;
  //resultFileName << storageDirectory << "/" << config;
  resultFileName << storageDirectory ;

  TSystemDirectory directory(resultFileName.str().c_str(),
                             resultFileName.str().c_str());
  gSystem->cd( directory.GetName() );

  TList *files = directory.GetListOfFiles();
  std::cout << directory.GetName() << std::endl;
  if (!files)
    return;

  // How many RTA root output files are in the directory
  int run = 0;
  int nRuns = 0;
  TSystemFile *file;
  TIter next(files);
  while ((file = (TSystemFile*) next())) {
    TString fname = file->GetName();
    //if(fname.EndsWith(".root") && fname.BeginsWith("Ring")) ++nRuns;
    if(fname.EndsWith(".root") && fname.BeginsWith(prefix))
    {
         ++nRuns;
    }
  }

  std::cout << nRuns << " runs need to be processed..." << std::endl;
  Plotters::SkyHist *onmap[nRuns];
  Plotters::SkyHist *onexpmap[nRuns];

  // define stuff globally
  int count = 0;
  std::map< std::pair<int,int>,std::map<int,double> > vAccs, vEvents, vAllowed, vDates;

  next.Begin();
  bool firstrun = true;
  //while ((file = (TSystemFile*) next())) {
  for (int nrun=0; nrun<nRuns; nrun++) {
    //TString fname = file->GetName();
   //std::cout << fname << std::endl;
    //if (file->IsDirectory() || !fname.EndsWith(".root") || !fname.BeginsWith(prefix))
    //  continue;
    std::stringstream fnamestream; //This is done to use the run in the chronological order
    fnamestream << "run_" << nrun << "_" << config << ".root";
    TString fname = fnamestream.str();
    if (!fname)
        {
            ++nrun;
            continue;
        }
    else{
    ProcessOutputFile(fname, fCurrentStats, fCurrentMaps);

    // ignore CT1-4 runs
    if (withCT5only && fCurrentStats->GetTelescopePattern() < 32 ) continue;
    std::cout << "Processing run " << run << " with run number "<< fname << " and filename " << fname << std::endl;

    int runnum = fCurrentStats->GetIdNumber();
   std::cout << "Run: " << runnum << std::endl;
    double livetime = fCurrentStats->GetRateStats().GetLiveTime();
   std::cout << "livetime = " << livetime << std::endl;

    if( runnum < minrun ) continue;
    //if(livetime < 10*60 && livetime > 1e-6)
    if(livetime < 10*60)
      {
         cout << "=========================== Run " << runnum << " was rejected since its livetime is too small!" << endl;
         continue;
      }

   //Retrieving the maps and information for each runs
   //
    if(firstrun) {
      fTotalStats->ReplaceStatsFrom(*fCurrentStats, true);
      fTotalMaps->ReplaceMapsFrom(*fCurrentMaps);
      firstrun = false;
    } else {
      fTotalStats->AddStatsFrom(*fCurrentStats, true);
      fTotalMaps->AddMapsFrom(*fCurrentMaps);
    }

    onmap[run] = (Plotters::SkyHist*)&fCurrentMaps->GetOnMap();
    onexpmap[run] = (Plotters::SkyHist*)&fCurrentMaps->GetOnExposureMap();

    // correlate with theta before doing the test
    onmap[run] = onmap[run]->GetCorrelatedSkyMap(theta);
    onexpmap[run] = onexpmap[run]->GetCorrelatedSkyMap(theta);

    for(int ii = 1; ii<= onmap[run]->GetNbinsX(); ++ii) {
      for(int jj = 1; jj<=onmap[run]->GetNbinsY(); ++jj) {

   double onevents = onmap[run]->GetBinContent(ii,jj);
   double acc = onexpmap[run]->GetBinContent(ii,jj);
   double mjd_start = fCurrentStats->GetRateStats().startTime.GetUTC().GetModifiedJulianDate();
   // double mjd_end = mjd_start + run + 0.5;
   double mjd_end = fCurrentStats->GetRateStats().endTime.GetUTC().GetModifiedJulianDate();
   // if(ii == 125 && jj == 125)
   //   std::cout << onevents << " " << acc << " " << mjd_start << " " << mjd_end << std::endl;

   if(acc > 0) {
     vDates[std::make_pair(ii,jj)][run] = (mjd_start + mjd_end)/2.;
     vEvents[std::make_pair(ii,jj)][run] = onevents;
     vAccs[std::make_pair(ii,jj)][run] = acc;
     vAllowed[std::make_pair(ii,jj)][run] = 1;
     ++count;
   }
      }
    }
    std::cout << "Processed run " << run << " with run number " << runnum << std::endl;
    ++run;
  }}

  // do the test now
  Plotters::SkyHist* hONOFFTestMap = (Plotters::SkyHist*)onmap[0]->Clone("ONOFFTest");
  Plotters::SkyHist* hONOFFTestMap_Post = (Plotters::SkyHist*)onmap[0]->Clone("ONOFFTest_Post");
  hONOFFTestMap->Reset();
  hONOFFTestMap_Post->Reset();

  std::vector<TH1F*> histos, histos2;

  for(int i = 0; i < 7; ++i) {
    histos.push_back(new TH1F);
    histos2.push_back(new TH1F);
  }

  // define testpos
  int test_i, test_j;
  hONOFFTestMap_Post->FindBinPosition(hONOFFTestMap_Post->GetPosition(),test_i,test_j); //Getting the position of the pixel on which the source is centered

  int nx = hONOFFTestMap_Post->GetNbinsX();
  int last_step = 0;
  for(int ii = 1; ii <= hONOFFTestMap->GetNbinsX(); ++ii) {  //Looping over all the pixels, to retrieve the relevant information and computing sig for each pixel
    if(((ii-1)*20 / (nx-1)) > last_step) {
      last_step = ((ii-1)*20 / (nx-1));
      // std::cout << "i>" << ii - 1 << " (-> " << nx - 1 << ")" << std::endl;
      std::cout << "." << std::flush;
    }
   
   //Computing the corrected significance for each pixel over all the runs. 
    for(int jj = 1; jj <= hONOFFTestMap->GetNbinsY(); ++jj) {

      std::map<int,double> vDate_In = vDates[std::make_pair(ii,jj)];  //Getting the relevant information for each pixel for all runs
      std::map<int,double> vAccs_In = vAccs[std::make_pair(ii,jj)];
      std::map<int,double> vEvents_In = vEvents[std::make_pair(ii,jj)];
      std::map<int,double> vAllowed_In = vAllowed[std::make_pair(ii,jj)];
      
     int vecsize = 0;
     double maxsig = 0.;
     maxsig = get_var_sigma(vDate_In, vAccs_In, vEvents_In, vAllowed_In, vecsize, false, histos, "");
     if(test_i == ii && test_j == jj) {
         maxsig = get_var_sigma(vDate_In, vAccs_In, vEvents_In, vAllowed_In, vecsize, true, histos, "TestPos_");
    std::cout << "Processed " << vecsize << " runs" << std::endl;
      }
      double maxsig_posttrials = 0;

      if(maxsig != 0){
    if(maxsig > 8.3)
      maxsig_posttrials = TMath::ErfcInverse(TMath::Erfc(maxsig*1./sqrt(2))*vecsize)*sqrt(2);
    else
      maxsig_posttrials = TMath::ErfcInverse(1-pow(1-TMath::Erfc(maxsig*1./sqrt(2)),vecsize))*sqrt(2);
      }
      
      hONOFFTestMap->SetBinContent(ii,jj,maxsig);
      hONOFFTestMap_Post->SetBinContent(ii,jj,maxsig_posttrials);
      
    }
}

  int max_i, max_j, max_k;
  hONOFFTestMap_Post->GetMaximumBin(max_i,max_j,max_k);

  std::map<int,double> vDate_In = vDates[std::make_pair(max_i,max_j)];
  std::map<int,double> vAccs_In = vAccs[std::make_pair(max_i,max_j)];
  std::map<int,double> vEvents_In = vEvents[std::make_pair(max_i,max_j)];
  std::map<int,double> vAllowed_In = vAllowed[std::make_pair(max_i,max_j)];

  int vecsize = 0;
  double sigma = get_var_sigma(vDate_In, vAccs_In, vEvents_In, vAllowed_In, vecsize, true, histos2, "MaxPos_");

  double maxsigma = TMath::ErfcInverse(1-pow(1-TMath::Erfc(sigma*1./sqrt(2)),vecsize))*sqrt(2);

  std::cout << "Maximum variability of " << maxsigma << " at coordinate " 
        << hONOFFTestMap_Post->GetBinCoordinate(max_i,max_j) << std::endl;

  TCanvas *c = new TCanvas("ONOFF","ONOFF",1000,500);
  c->Divide(2,1);
  c->cd(1);
  hONOFFTestMap->Draw("colz");
  c->cd(2);
  hONOFFTestMap_Post->SetTitle("BgMaps_RingBgMaker_Current_OnMap_Post");
  hONOFFTestMap_Post->Draw("colz");

  TCanvas *cONOFFTest = new TCanvas("cONOFF","cONOFF",1400,600);
  cONOFFTest->Divide(3,1);
  cONOFFTest->cd(1);
  histos[0]->Draw();// on
  histos[1]->SetLineColor(2);
  histos[1]->Draw("same"); // alpha*off
  histos[2]->SetLineColor(4); // excess
  histos[2]->Draw("same");
  TLegend *leg = new TLegend(0.15,0.75,0.4,0.85);
  leg->AddEntry(histos[0],"ON","l");
  leg->AddEntry(histos[1],"alpha * OFF","l");
  leg->AddEntry(histos[2],"Excess","l");
  leg->SetFillColor(0);
  leg->Draw();

  cONOFFTest->cd(2);
  histos[3]->Draw(); // sigma
  histos[4]->SetLineColor(7);// region allowed
  histos[4]->Draw("same");
  TLegend *leg2 = new TLegend(0.15,0.75,0.4,0.85);
  leg2->AddEntry(histos[3],"Significance","l");
  leg2->AddEntry(histos[4],"Exclusions","l");
  leg2->SetFillColor(0);
  leg2->Draw();

  cONOFFTest->cd(3)->SetLogy();
  histos[6]->SetLineColor(2);// sign-dist allowed
  histos[6]->Draw();
  histos[5]->Draw("same"); // sign-dist
  histos[6]->Fit("gaus","","SAMES",-5,10);      
  TF1 *f = histos[6]->GetFunction("gaus");
  if(f) f->SetLineColor(kRed+2);


  TCanvas *cONOFFTest2 = new TCanvas("cONOFF2","cONOFF2",1400,600);
  cONOFFTest2->Divide(3,1);
  cONOFFTest2->cd(1);
  histos2[0]->Draw();// on
  histos2[1]->SetLineColor(2);
  histos2[1]->Draw("same"); // alpha*off
  histos2[2]->SetLineColor(4); // excess
  histos2[2]->Draw("same");
  TLegend *leg11 = new TLegend(0.15,0.75,0.4,0.85);
  leg11->AddEntry(histos2[0],"ON","l");
  leg11->AddEntry(histos2[1],"alpha * OFF","l");
  leg11->AddEntry(histos2[2],"Excess","l");
  leg11->SetFillColor(0);
  leg11->Draw();

  cONOFFTest2->cd(2);
  histos2[3]->Draw(); // sigma
  histos2[4]->SetLineColor(7);// region allowed
  histos2[4]->Draw("same");
  TLegend *leg22 = new TLegend(0.15,0.75,0.4,0.85);
  leg22->AddEntry(histos2[3],"Significance","l");
  leg22->AddEntry(histos2[4],"Exclusions","l");
  leg22->SetFillColor(0);
  leg22->Draw();

  cONOFFTest2->cd(3)->SetLogy();
  histos2[6]->SetLineColor(2);// sign-dist allowed
  histos2[6]->Draw();
  histos2[5]->Draw("same"); // sign-dist
  histos2[6]->Fit("gaus","","SAMES",-5,10);      
  TF1 *f2 = histos2[6]->GetFunction("gaus");
  if(f2) f2->SetLineColor(kRed+2);

  TString name; //This is done to use the run in the chronological order
  name = "ONOFF_Map_";
  name += prefix; 
  name += ".png"; 

  c->SaveAs(name);

  name = "ONOFF_TestPos_";
  name += prefix; 
  name += ".png"; 
  cONOFFTest->SaveAs(name);

  name = "ONOFF_MaxPos_";
  name += prefix; 
  name += ".png"; 
  cONOFFTest2->SaveAs(name);

  return;
  
}


void ProcessOutputFile(TString fname, 
             Background::BgStats* fCurrentStats, 
             Background::BgMaps* fCurrentMaps) {

  TFile resultFile(fname, "READ");
  if (!resultFile.IsOpen())
    {return;
   std::cout << "Could not open the file: " << fname << std::endl;
   }
  
  // Read in runheader from file and create event selection object
  Sash::HESSArray* hess = &Sash::HESSArray::GetHESSArray();
  Sash::DataSet *run = Background::FindRunDataSet();
  run->GetEntry(0);

  fCurrentStats = hess->Handle<Background::BgStats>("RingBgMaker_Current");
  fCurrentMaps  = hess->Handle<Background::BgMaps>("RingBgMaker_Current");

  Plotters::SkyHist *on = (Plotters::SkyHist*)&fCurrentMaps->GetOnMap();
  Plotters::SkyHist *onex = (Plotters::SkyHist*)&fCurrentMaps->GetOnExposureMap();

  std::cout << (*on->GetPosition().GetSystem()) << std::endl;
  int r = fCurrentStats->GetIdNumber();
  
////  Background::BgMaps *storageMaps = 
////    hess->Handle<Background::BgMaps>("BgMaps_RingBgMaker_Storage");
////  Background::BgStats *storageStats = 
////    hess->Handle<Background::BgStats>("BgMaps_RingBgMaker_Storage");
////  gROOT->cd();
//std::cout << "we get here4..." << std::endl;
//
//  // Set Telescope Pattern and Run Number for this run by hand
////  storageStats->GetTelescopePattern() = eventsel->GetTelSelectedPattern();
////  fCurrentStats->GetTelescopePattern() = eventsel->GetTelSelectedPattern();
////  storageStats->GetTelescopePattern() = fCurrentStats->GetTelescopePattern();
//  fCurrentStats->GetTelescopePattern() = fCurrentStats->GetTelescopePattern(); 
//std::cout << "we get here5..." << std::endl;
// // storageStats->GetIdNumber() = fCurrentStats->GetIdNumber();;
//  fCurrentStats->GetIdNumber() = fCurrentStats->GetIdNumber();
//std::cout << "we get here6..." << std::endl;
//
//  // Replace Maps
//  //fCurrentStats->ReplaceStatsFrom(*storageStats, true);
//  //fCurrentMaps->ReplaceMapsFrom(*storageMaps);
//  fCurrentStats->ReplaceStatsFrom(*fCurrentStats, true);
//std::cout << "we get here7..." << std::endl;
//  fCurrentMaps->ReplaceMapsFrom(*fCurrentMaps);
//std::cout << "we get here8..." << std::endl;
  
  return;
}
