void GenerateTransientTest(TString directory,TString config, double theta)
{
   gROOT->ProcessLine(".x $HESSROOT/rootlogon.C");
   gROOT->ProcessLine(".L make_transient_nightly.C+");
   gROOT->ProcessLine(".L make_transient_test.C+");
   make_transient_nightly("run", directory.Data(), config.Data(), 10000, theta, false);
   make_transient_test("run", directory.Data(), config.Data(), 10000, theta, false);
}
