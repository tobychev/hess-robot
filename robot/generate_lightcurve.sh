#/usr/bin/env bash
if test "$BASH" = "" || "$BASH" -uc "a=();true \"\${a[@]}\"" 2>/dev/null; then
    # Bash 4.4, Zsh
    set -euo pipefail
else
    # Bash 4.3 and older chokes on empty arrays with set -u.
    set -eo pipefail
fi
shopt -s nullglob globstar

# -allow a command to fail with !’s side effect on errexit
# -use return value from ${PIPESTATUS[0]}, because ! hosed $?
! getopt --test > /dev/null 
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
    echo 'I’m sorry, `getopt --test` failed in this environment.'
    exit 1
fi

if ! python -c 'import sys; assert sys.version_info >= (3,6)' 2> /dev/null; then
	if [ "$HOSTNAME" == "lfs1.mpi-hd.mpg.de" ]; then
		export PATH=/home/hfm/hess/software/root/latest/bin:/home/hfm/hess/software/python/miniconda/miniconda/bin:/home/hfm/hess/hap-18-11//scons/local/:/home/hfm/hess/hap-18-11//bin:/nfs/d22/hfm/hess/Base_Software_Linux64_gcc4.8a/usr/bin:/nfs/d22/hfm/hess/Base_Software_Linux64_gcc4.8a/bin:.:/home/extern/tobychev/bin:/usr/local/bin:/bin:/usr/bin:/opt/gnome/bin:/usr/bin/X11:/home/hfm/hess/hap-18-11//hddst/bin:/usr/local/sbin:/usr/sbin:/home/extern/tobychev/bin
		export PYTHONPATH=/home/hfm/hess/software/root/latest/lib:/nfs/d22/hfm/hess/Base_Software_Linux64_gcc4.8a/usr/bin/../root/lib:/home/hfm/hess/hap-18-11//dash:/home/hfm/hess/hap-18-11//hesspy
	fi
	if [ "$HOSTNAME" == "lb-00009" ]; then
		conda activate base
	fi
fi

usage ()
{
    echo "usage: generate_lightcurve.sh sources.ini [-d] [options]"
    echo "  First argument must be the source ini file"
    echo "  Internal tool, not really for manual use "
	 echo "  -d                                  debug"
    exit 1	
}

STARTDIR="$(pwd)"


#  First thing given must be the source ini file
SRCINI="$1"
shift


LONGOPTS=debug,loc:,srcdir:,srcname:

# -regarding ! and PIPESTATUS see above
# -temporarily store output to be able to check for errors
# -activate quoting/enhanced mode (e.g. by writing out “--options”)
# -pass arguments only via   -- "$@"   to separate them correctly
! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # e.g. return value is 1
    #  then getopt has complained about wrong arguments to stdout
    exit 2
fi
# read getopt’s output this way to handle the quoting right:
eval set -- "$PARSED"

debug=n dir=n name=n
srcDir=- srcName=- robotdir=- 
# now enjoy the options in order and nicely split until we see --

while true; do
    case "$1" in
        --debug)
            debug=y
            shift
            ;;
        --loc)
            robotdir="$2"
            shift 2
            ;;
        --srcdir)
				dir=y
            srcDir="$2"
            shift 2
            ;;
        --srcname)
            name=y
            srcName="$2"
            shift 2
            ;;
        --)
            shift
				if [ ${#PARSED} -eq 3 ]; then
					usage
				fi
				if [ ! -z "${1+x}" ]; then
					echo "Unknown option $1"
					usage
				fi
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

## Parse source config section ##
ops=$(python $robotdir/robot/hess_robot.py "$SRCINI" --parse-section-options "$srcName" "HapConfig" "OnRegion"  "StartSoftwareDir" )
sep='#'
source_options=()
while read -rd "$sep" i; do
    source_options+=("$i")
done < <(printf '%s%s' "$ops" "$sep")

rootfiledir="$robotdir/robot/rootrobot"
hapConf="${source_options[0]}"
theta="${source_options[1]}"
export STARTSOFTDIR="${source_options[2]}"



if [  -d "$srcDir/report_files" ]; then

   if [ ! -d "$srcDir/lightcurve_dir" ]; then
      mkdir "$srcDir/lightcurve_dir"
   fi

   if [ "$debug" == y ]; then
	   printf "Copying root scripts to %s/lightcurve_dir\n" "$srcDir"
   fi
   cp "$rootfiledir"/GenerateLigthcurve.C "$rootfiledir"/GenerateTransientTest.C "$rootfiledir"/make_transient_test.C "$rootfiledir"/make_transient_nightly.C "$srcDir/lightcurve_dir"

   if [ "$debug" == y ]; then
	   printf "Collecting event and result files into %s/lightcurve_files using symlinks\n" "$srcDir"
      printf "Doing python %s/robot/hess_robot.py '%s' --generate-lightcurve-directory '%s' '%s/lightcurve_dir'\n" "$robotdir" "$SRCINI" "$srcName" "$srcDir"
   fi
   python $robotdir/robot/hess_robot.py "$SRCINI" --generate-lightcurve-directory "$srcName" "$srcDir/lightcurve_dir"
   

   cd "$srcDir/lightcurve_dir" 

   if [ "$debug" == y ]; then
      printf "done python \n"   
      printf "Doing root -b -q GenerateTransientTest.C(\"%s/lightcurve_dir\",\"%s\",%s) > results_Run_<date>.log\n"	 "$srcDir" "$hapConf" "$theta"
   fi
   root -b -q "GenerateTransientTest.C(\"$srcDir/lightcurve_dir\",\"$hapConf\",$theta)" > results_Run_$(date -I'minutes').log 
   if [ $? -ne 0 ]; then
      printf "Root failed to run cleanly\n"
      exit -1
   fi

   if [ "$debug" == y ]; then
      printf "done Transient Test \n"
      printf "startsoftdir: %s" "$STARTSOFTDIR"
      printf "Doing root -b -q \"GenerateLigthcurve.C(\"%s\",\"../%s_link_merged.root\")\"\n" "$srcName" "$srcName"
   fi
   root -b -q "GenerateLigthcurve.C(\"$srcName\",\"../${srcName}_link_merged.root\")"

   if [ $? -ne 0 ]; then
      printf "Root failed to run cleanly\n"
      exit -1
   fi

else
   printf "No directory with results found at %s" "$srcDir/lightcurve_dir\n"
   exit -1
fi

exit 0
