#!/usr/bin/env bash
if test "$BASH" = "" || "$BASH" -uc "a=();true \"\${a[@]}\"" 2>/dev/null; then
   # Bash 4.4, Zsh
    set -euo pipefail
	 set -o errexit -o noclobber -o nounset
else
   # Bash 4.3 and older chokes on empty arrays with set -u.
    set -eo pipefail
fi

shopt -s nullglob globstar

# -allow a command to fail with !’s side effect on errexit
# -use return value from ${PIPESTATUS[0]}, because ! hosed $?
! getopt --test > /dev/null 
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
    echo 'I’m sorry, `getopt --test` failed in this environment.'
    exit 1
fi

if ! python -c 'import sys; assert sys.version_info >= (3,6)' 2> /dev/null; then
	if [ "$HOSTNAME" == "lfs1.mpi-hd.mpg.de" ]; then
		export PATH=/home/hfm/hess/software/root/latest/bin:/home/hfm/hess/software/python/miniconda/miniconda/bin:/home/hfm/hess/hap-18-11//scons/local/:/home/hfm/hess/hap-18-11//bin:/nfs/d22/hfm/hess/Base_Software_Linux64_gcc4.8a/usr/bin:/nfs/d22/hfm/hess/Base_Software_Linux64_gcc4.8a/bin:.:/home/extern/tobychev/bin:/usr/local/bin:/bin:/usr/bin:/opt/gnome/bin:/usr/bin/X11:/home/hfm/hess/hap-18-11//hddst/bin:/usr/local/sbin:/usr/sbin:/home/extern/tobychev/bin
		export PYTHONPATH=/home/hfm/hess/software/root/latest/lib:/nfs/d22/hfm/hess/Base_Software_Linux64_gcc4.8a/usr/bin/../root/lib:/home/hfm/hess/hap-18-11//dash:/home/hfm/hess/hap-18-11//hesspy
	fi
	if [ "$HOSTNAME" == "lb-00009" ]; then
		conda activate base
	fi
fi

usage ()
{
    echo "usage: $0 [-d] [-g source config] "
	 echo "  -d                                  Enable various printouts"
    echo "  -g, --generate <source config>      Generate submision scripts from source config"
    echo "  -r, --run <source config>           Run submission scripts corresponding to the source config, generating them if necessary"
    echo "  -R, --report <source config>        Generate report from run results for sources listed in the source config"
    echo "  -l, --lightcurve <source config>    Generate ligtcurve from run results for sources listed in the source config"
    echo "  -f, --fits <source config>          Run fits export script for sources and analysis cuts listed in the source config"
    echo "  -c, --check <source config>         Check all runs completed for sources listed in the source config"
    exit 1	
}

require ()
{
hash "$@" || 127
}

require compgen

robotdir=$(dirname $(readlink -f $0))

OPTIONS=dg:r:R:l:c:f:
LONGOPTS=debug,extraparallel,generate:,run:,report:,lightcurve:,check:,fits:

# -regarding ! and PIPESTATUS see above
# -temporarily store output to be able to check for errors
# -activate quoting/enhanced mode (e.g. by writing out “--options”)
# -pass arguments only via   -- "$@"   to separate them correctly
! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # e.g. return value is 1
    #  then getopt has complained about wrong arguments to stdout
    exit 2
fi
# read getopt’s output this way to handle the quoting right:
eval set -- "$PARSED"

debug=n epar=n srcFile=- run=n gen=n report=n light=n check=n fits=n
parse_srcs=n
debug_str=""
# now enjoy the options in order and nicely split until we see --

while true; do
    case "$1" in
        -d|--debug)
            debug=y
            debug_str="--debug"
            shift
            ;;
        --extraparallel)
            epar=y
            shift
            ;;
        -g|--generate)
				gen=y
            srcFile="$2"
            shift 2
            ;;
        -r|--run)
            run=y
            parse_srcs=y
            srcFile="$2"
            shift 2
            ;;
        -R|--report)
            report=y
            parse_srcs=y
            srcFile="$2"
            shift 2
            ;;
        -l|--lightcurve)
				light=y
            parse_srcs=y
            srcFile="$2"
            shift 2
            ;;
        -f|--fits)
            fits=y
            parse_srcs=y
            srcFile="$2"
            shift 2
            ;;
        -c|--check)
            check=y
            parse_srcs=y
            srcFile="$2"
            shift 2
            ;;
        --)
            shift
				if [ ${#PARSED} -eq 3 ]; then
					usage
				fi
				if [ ! -z "${1+x}" ]; then
					echo "Unknown option $1"
					usage
				fi
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

if [ "$debug" == y ]; then
   echo "$PARSED"
   echo "Debug $debug"
   echo "Extra parallel $epar"
   echo "Generate $gen"
   echo "Make report $report"
   echo "src file $srcFile"
   echo "Parse src file $parse_srcs"
fi

if [ ! -e "$srcFile" ]; then
	echo "Input file not found: $srcFile"
	exit 1
fi

## Generate configs section ##
if [ "$gen" == "y" ]; then
	if [ "$debug" == y ]; then
		echo "python $robotdir/robot/hess_robot.py "$srcFile" --generate-all"
	fi
	python $robotdir/robot/hess_robot.py "$srcFile" --generate-all
fi

## Parse sources section ##
srcs=""
if [ "$parse_srcs" == "y" ]; then
	if [ "$debug" == y ]; then
		echo "python $robotdir/robot/hess_robot.py "$srcFile" --parse-sources"
	fi
   srcs=$(python $robotdir/robot/hess_robot.py "$srcFile" --parse-sources)
	if [ "$debug" == y ]; then
      echo "$srcs"
	fi
fi
# Need extra space to iterate over them properly
srcs="$srcs "

## Run analysis section ##
if [ "$run" == "y" ]; then
	scriptdir=$(awk -F "=" '/SubmitScriptDir/ {print $2}' $srcFile)
	scriptdir=$(readlink -f $scriptdir)

	# Iterate over the sources start here
	while read -rd " " i; do
		if [ ! -e "$scriptdir/submit_$i.sh" ]; then
			if [ "$debug" == y ]; then
				echo "python $robotdir/robot/hess_robot.py $srcFile --generate-only $i"
			fi
			python "$robotdir/robot/hess_robot.py" "$srcFile" --generate-only "$i"
		fi
		if [ "$debug" == y ]; then
			echo "Running $scriptdir/submit_$i.sh"
		fi
      # Launch submit script in a subshell 
      # optionally as background processes 
      # which should mean submissions happen
      # in parallel, who knows how that'll work
		if [ "$epar" == y ]; then
			echo "Attempting to run submission scripts in parallel, "
         ( "$scriptdir/submit_$i.sh&" )
      else
         ( "$scriptdir/submit_$i.sh" )
      fi

	done < <(printf '%s' "$srcs")
fi

## Check run result section ##
if [ "$check" == "y" ]; then
	# Iterate over the sources start here
	while read -rd " " name; do
      
		result=$(python "$robotdir/robot/hess_robot.py" "$srcFile" --check-runresult "$name")
		if [ "$debug" == y ]; then
         echo "Doing source $name"
		fi
      if [[ "$result" == *"errors"* ]]; then
         echo "$result"
         echo "Full log saved in check_runresult_${name}.log"
      fi 
      #TODO: Add code that takes out failes srouces from the $srcs list

	done < <(printf '%s' "$srcs")
fi

## Make lightcurve section ##
if [ "$light" == "y" ]; then
	resultdir=$(awk -F "=" '/ProjRootDir/ {print $2}' "$srcFile")
	resultdir=$(readlink -f $resultdir)


	# Iterate over the sources start here
	while read -rd " " name; do
      
      srcdir=$(printf "%s/%s" "$resultdir" "$name")
		if [ "$debug" == y ]; then
         echo "Doing source $name"
         echo "$robotdir/robot/generate_lightcurve.sh" "$srcFile" $debug_str "--loc $robotdir"  "--srcdir $srcdir"  "--srcname $name"
		fi

      ("$robotdir/robot/generate_lightcurve.sh" "$srcFile" $debug_str  --loc "$robotdir" --srcdir "$srcdir" --srcname "$name")
      state=$?
		if [ "$debug" == y ]; then
         if [ $state -ne  0 ]; then
            echo "Encountered an error doing report. Halting due to debug setting"
            exit -1
         fi
		fi
	done < <(printf '%s' "$srcs")
fi

## Make report section ##
if [ "$report" == "y" ]; then
	resultdir=$(awk -F "=" '/ProjRootDir/ {print $2}' "$srcFile")
	resultdir=$(readlink -f $resultdir)

	# Iterate over the sources start here
	while read -rd " " name; do
      
		if [ "$debug" == y ]; then
         echo "Doing source $name"
         echo "$robotdir/robot/generate_report.sh $srcFile $debug_str --loc $robotdir --srcdir $resultdir/$name --srcname $name"
		fi
      srcdir=$(printf "%s/%s" "$resultdir" "$name")
      # Very deliberate skip of " around debug_str: using them it will lead to an error when not running debug mode 
      output=$( "$robotdir/robot/generate_report.sh" "$srcFile" $debug_str --loc "$robotdir" --srcdir "$srcdir" --srcname "$name" )
      state=$?
		if [ "$debug" == y ]; then
         if [ $state -ne  0 ]; then
            echo "Encountered an error doing report. Halting due to debug setting"
            exit -1
         fi
		fi

	done < <(printf '%s' "$srcs")
fi

## Do fits export section ##
if [ "$fits" == "y" ]; then
	resultdir=$(awk -F "=" '/ProjRootDir/ {print $2}' "$srcFile")
	resultdir=$(readlink -f $resultdir)

	# Iterate over the sources start here
	while read -rd " " name; do
      
      srcdir=$(printf "%s/%s" "$resultdir" "$name")
		if [ "$debug" == y ]; then
         echo "Doing source $name"
         echo "Running command: $robotdir/robot/generate_fits.sh $srcFile $debug_str --loc $robotdir --srcdir $srcdir --srcname $name"
		fi

      ("$robotdir/robot/generate_fits.sh" "$srcFile" $debug_str --loc "$robotdir" --srcdir "$srcdir" --srcname "$name")
      state=$?
		if [ "$debug" == y ]; then
         if [ $state -ne  0 ]; then
            echo "Encountered an error during fits export. Halting due to debug setting"
            exit -1
         fi
		fi
	done < <(printf '%s' "$srcs")
fi
